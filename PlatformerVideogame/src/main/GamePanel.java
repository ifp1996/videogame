package main;

//Using '*' imports every Tool listed inside the category we're looking on.

import java.awt.*;

import gamestate.GameStateManager;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable, KeyListener{

	private static final long serialVersionUID = 1762727615551671586L;
	
	// Dimensions
	public static final int WIDTH = 320;
	public static final int HEIGHT = 240;
	public static final int SCALE = 2;
	
	// Thread
	
	private Thread thread;
	private boolean running;
	private int FPS = 60;
	private long targetTime = 1000 / FPS;
	
	// Image
	
	private BufferedImage image;
	private Graphics2D g;
	
	//Game State Manager
	private GameStateManager gsm;
	
	public GamePanel() {
		super(); //Sets a container. These are made to include other components inside of such. Pretty much a "subclass".
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setFocusable(true);
		requestFocus();
	}
	
	public void addNotify() {
		super.addNotify();
		if(thread == null) {
			thread = new Thread(this);
			addKeyListener(this);
			thread.start(); //This container generates a new thread and runs it if there's none. Plus, adds a Key Listener to allow the user to make input.
		}
	}
	
	private void init() {
		
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		
		g = (Graphics2D) image.getGraphics();
		
		running = true;
		
		gsm = new GameStateManager();
		
	}
	
	public void run() {
		
		init();
		//'Long' is a 64 bit integer that is meant for storing higher number values and decreased negative numbers.
		long lastTime = System.nanoTime();; //lastTime will work as an 'acc'
		final double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		
		
		// Loop
		while(running) {
		
			long now = System.nanoTime();
			delta += (lastTime - now) / ns;
			lastTime = now;
			
			if(delta >= 1) {
				tick();
				delta--;
			}
			render();
			
			}
		
			
		}
	
	
	private void tick() {
		
	}

	private void render() {
		
	}
	
	private void update() {
		gsm.update();
	}
	private void draw() {
		gsm.update();
	}
	private void drawToScreen() {	
		
		Graphics g2 = getGraphics(); //GamePanel graphics object
		g2.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		g2.dispose();
	
	}

	public void keyTyped(KeyEvent key) {}
	public void keyPressed(KeyEvent key) {
		gsm.keyPressed(key.getKeyCode());
	}
	public void keyReleased(KeyEvent key) {
		gsm.keyReleased(key.getKeyCode());
	}
	
}
