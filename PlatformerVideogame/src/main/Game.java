package main;

import javax.swing.JFrame;

public class Game {
	
	public static void main(String[] args) {
		
		JFrame window = new JFrame("Game Title"); //JFrame creates a window for game representation. The value inside the declared JFrame is the Title of the game's window.
		window.setContentPane(new GamePanel()); //Merely sets panel for the videogame.
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //This function right here causes the game to fully close when the window is "killed"
		window.setResizable(false); //This function is set to unallow the user to resize the window.
		window.pack(); //Automatic resize function, according to the dimensions the screen has set.
		window.setVisible(true); //Boolean that sets the Window visible.
		
	}
}
