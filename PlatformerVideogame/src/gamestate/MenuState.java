package gamestate;

import tilemap.Background;

import java.awt.*;
import java.awt.event.KeyEvent;


public class MenuState extends GameState{

	private Background bg;
	
	//Main Menu text and buttons
	
	private int currentChoice = 0;
	private String[] menu = {
		"START",
		"SETTINGS",
		"EXIT"
	};
	//Since I have no image to represent as a "Logo", I'm just leaving a colored text as a Title.
	private Color titleColor;
	private Font titleFont;
	
	private Font font;
	
	public MenuState(GameStateManager gsm) {
		
		this.gsm = gsm;
		
		try {
			
			bg = new Background("/backgrounds/menubg.PNG", 1); //Set background file for menu.
			bg.setVector(-0.1, 0);
			
			titleColor = new Color(128, 0, 0); //Roho pasiom. Porque s�.
			titleFont = new Font("Georgia", font.PLAIN, 28);
			
			font = new Font("Verdana", Font.PLAIN, 12);
			
		}
		catch(Exception e) {
			e.printStackTrace();;
		}
	}
	
	public void init() {}
	public void update() {
		bg.update();
	}
	public void draw(Graphics2D g) {
		//Draw Background
		bg.draw(g);
		
		//Draw Title
		g.setColor(titleColor);
		g.setFont(titleFont);
		g.drawString("Test", 80, 70);
		
		//Draw Menu
		g.setFont(font);
		for (int i = 0; i < menu.length; i++) {
			if(i == currentChoice) {
				g.setColor(Color.BLACK);
			}

			else {
				g.setColor(Color.RED);
			}
			g.drawString(menu[i], 145, 140 + i * 15);
		}
	}
	
	private void select() {
		if(currentChoice == 0) {
			//Supposed to Start the game.
		}
		if(currentChoice == 1) {
			//Settings
		}
		if(currentChoice == 2) {
			System.exit(0);
		}

	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER) {
			select();
		}
		if(k == KeyEvent.VK_UP) {
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = menu.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == menu.length) {
				currentChoice = 0;
			}
		}
	}
	public void keyReleased(int k) {}
	
}
